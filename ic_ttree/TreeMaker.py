from ROOT import TFile, TTree, TObject
from array import array
from tree_common import *


class TreeMaker:
  """A wrapper to make storing data in a TTree easy.

  Usage:
    tree = TreeMaker('myfile.root', 'mytree', branches)
    This will create a tree called mytree in a new file called myfile.root with branches
    defined being an array of either strings or dictionaries, with each
    string being the name of a branch (will default the branch to being a single doule)
    and the dictionary allowing configuration of the branch in detail.

    The branch dictionary can include the name (string), type(string or predefined,
    constant), length(integer to make fixed length, or string with a branch containing
    the length), max length(integer to define the max length of a variable length array),
    object if the variable should contain an object. This object should be persisted
    until the tree is closed, and modifications to the object will be pushed to the tree
    when Fill() is called.
    

    If you want to append values to the branches of an existing tree add append=True as a
    constructor argument. To add extra branches to an existing tree add expand=True as a 
    constructor argument. In both cases, if the tree or file doesn't exist it is created.
"""

  def __init__(self, filename, treename, branches, append=False, expand=False):
    self._expand = expand
    self._append = append

    if self._expand or self._append:
      self._ofile = TFile(filename, 'update')
      self._otree = self._ofile.Get(treename)
      if not self._otree:
        self._otree = TTree(treename, treename)
    else:
      self._ofile = TFile(filename, 'recreate')
      self._otree = TTree(treename, treename)
    self._branch = {}
    for branch in branches:
      self.add(branch)
    
  def __enter__(self):
    """Impements the entry point for a with call"""

    return self
  
  def __exit__(self, type, value, tb):
    """Impements the exit point for a with call, this closes the file"""

    self.close()

  def add(self, branch):
    """Adds a new branch to the tree using the same dictionary format as in the constructor"""

    try:
      name = branch['name']
      try:
        type = ROOTtypeToArrayType(branch['type'])
      except:
        type = ROOTtypeToArrayType(DOUBLE)

      lenvar = None
      try:
        length = int(branch['length'])
      except ValueError:
        length = int(branch['max length'])
        lenvar = branch['length']
      except KeyError:
        length = 1
    except:
      name = branch
      type = ROOTtypeToArrayType(DOUBLE)
      length = 1

    if type == OBJECT:
      self._branch[name]  = branch['object']
    else:
      self._branch[name]  = array(type, [0]*length)

    if length == 1:
      if type != OBJECT:
        self._otree.Branch(name, self._branch[name], name+'/'+type)
      else:
        self._otree.Branch(name, self._branch[name])
    else:
      if lenvar:
        self._otree.Branch(name, self._branch[name], name+'['+str(lenvar)+']/'+type)
      else:
        self._otree.Branch(name, self._branch[name], name+'['+str(length)+']/'+type)
    
  def close(self):
    """Writes the TTree to the file and closes the file"""

    if self._expand or self._append:
      self._ofile.Write("", TObject.kOverwrite)
    else:
      self._ofile.Write()
    self._ofile.Close()
    
  def set(self, branch, value):
    """Sets the value of a branch. Does not fill the tree until Fill is called"""

    try:
      branch = self._branch[branch]
    except KeyError:
      print('No branch named', branch, 'exists')
      return
    try:
      for i in range(min(len(value), len(branch))):
        branch[i] = value[i]
    except TypeError:
      branch[0] = value

  def Fill(self, vars={}):
    """Sets the branches named in the dictionary vars to the values give, and pushs them to the tree"""

    for branch in vars:
      self.set(branch, vars[branch])
    if self._expand:
      for branch in self._branch:
        self._otree.GetBranch(branch).Fill()
    else:
      self._otree.Fill()
    
  def GetEntries(self):
    """Returns the current number of entries in the tree"""

    return self._otree.GetEntries()

  def file(self):
    """Returns the TFile object storing the tree"""

    return self._ofile

  def tree(self):
    """Returns the TTree object storing the data"""

    return self._otree


