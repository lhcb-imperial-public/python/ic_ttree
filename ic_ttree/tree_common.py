DOUBLE    = 'D'
FLOAT     = 'F'
BOOL      = 'O'
BYTE      = 'B'
UBYTE     = 'b'
SHORT     = 'S'
USHORT    = 's'
INT       = 'I'
UINT      = 'i'
LONG      = 'G'
ULONG     = 'g'
LONGLONG  = 'L'
ULONGLONG = 'l'
OBJECT    = 'object'

ARRAY_DOUBLE    = 'd'
ARRAY_FLOAT     = 'f'
ARRAY_BOOL      = 'b'
ARRAY_BYTE      = 'b'
ARRAY_UBYTE     = 'B'
ARRAY_SHORT     = 's'
ARRAY_USHORT    = 'S'
ARRAY_INT       = 'i'
ARRAY_UINT      = 'I'
ARRAY_LONG      = 'l'
ARRAY_ULONG     = 'L'
ARRAY_LONGLONG  = 'q'
ARRAY_ULONGLONG = 'Q'

def ROOTtypeToArrayType(ROOTtype):
  try:
    return { DOUBLE:    ARRAY_DOUBLE,    FLOAT: ARRAY_FLOAT, BOOL:   ARRAY_BOOL,   BYTE: ARRAY_BYTE,
             UBYTE:     ARRAY_UBYTE,     SHORT: ARRAY_SHORT, USHORT: ARRAY_USHORT, INT: ARRAY_INT,
             UINT:      ARRAY_UINT,      LONG:  ARRAY_LONG,  ULONG:  ARRAY_ULONG,  LONGLONG: ARRAY_LONGLONG,
             ULONGLONG: ARRAY_ULONGLONG}[ROOTtype]
  except:
    return ROOTtype

