# TTree manipulation tools #

This is a python package containing some tools to manipulate ROOT TTrees. Specifically to aid in their reading and writing.

## Installation ##

To install this package one can use pip:

`pip install git+https:https://gitlab.cern.ch/lhcb-imperial-public/python/ic_ttree.git`

## Usage ##

### TreeMaker ###

This tool makes writing TTrees easy. It handles scalar branches and 1D arrays of built in types, or single ROOT objects.

The constructor takes the following arguments `TreeMaker(filename, treename, branches, append=False, extend=False)`
* filename
  * This is the name of the file to store the tree in
  * This argument is required
* treename
  * This is the name of the tree to store data in
  * This argument is required
* branches
  * This is a list of branches in the tree to store data in (see below)
  * This argument is required
* append
  * Should an existing tree in the file be appended to
  * This argument is not required
  * This argument defaults to False
* extend
  * Should an existing tree had additional branches added
  * This argument is not required
  * This argument defaults to False

The branches list is a list of either branch names (which will be made as a single double by default), or a dictionary where you can specify how the branch should be configured.
The dictionary has the following entries:
* name
  * A string containing the name of the branch
  * This entry is required
* type
  * This entry is not required
  * Defaults to making doubles
  * Valid for built in types, or 'for a ROOT object
  * See Types below
* length
  * This should be convertable to an integer for fixed length arrays
  * This should be the name of another branch containing the length for variable length arrays
  * This entry is not required
  * Defaults to 1
* max length
  * This entry is required for variable length arrays
* object
  * Required only for the case where type is 'object'
  

#### Member functions ####

The `TreeMaker` has several member functions:

* `add(branch)`
  * Adds a single branch to the tree
  * `branch` should be in the same format as the entries used in the constructor
* `close()`
  * Finalises the tree and safely closes it
* `Fill(vars={})`
  * Fills the tree
  * `vars` should be a dictionary of the form `{'<branch name>':value}`
  * Any branches not provided values in `vars` will take them from the previous invocation, or entered using `set` are added to the tree depending on which call was most recent
* `GetEntries()`
  * Returns the current number of entries in the tree
* `set(branch, value)`
  * Sets the value of the named `branch` to the given `value`
  * This does not push the item into the tree, a call to `Fill` is still required once all desired branches have been set

To save the data to the tree a call to `close()` should be made. This is done automatically if the tree is filled in a with statement, and so is recommended.

#### Example usage ####

The following example creates a tree in a new file, and fills a few branches.

        from ic_ttree import TreeMaker

        branches = [Mass,
                    {'name':'IDs', 'length':10, type:'I'},
                    {'name':'Points', 'length':'NPoints', max length':10, type:'D'},
                    {'name':'NPoints', type:'I'},
                   ]

        
        with TreeMaker('myfile.root', 'mytree', branches) as tree:
          vars = {}
          vars['Mass'] = 5.285
          vars['ID'] = [1,2,3,4]
          vars['Points'] = [1.1, 2.2, 3.3]
          vars['NPoints'] = len(vars['Points'])
          tree.Fill(vars)


### TreeWrapper ###

This tool makes reading and iterating TTrees easy.


The constructor takes the following arguments `TreeWrapper(filename, treename)`
* filename
  * This is the name of the file to store the tree in
  * It can be a single string with the path to the file to read, or a list of string for files to chain together
  * This argument is required
* treename
  * This is the name of the tree to read data from
  * This argument is required


#### Member functions ####

The `TreeWrapper` has several member functions:

* addAlias(self, new, old)
  * Adds and alias for a branch (or alias). `new` is a string for the alias name, `old` is the name the alias should point to
* addFile(filename)
  * Appends a file to the tree chain from the file point to by `filename`
* addFriend(friend)
  * Adds a friend tree to to the tree, `friend` can either be a file name containing the friend tree, or can be the tree object itself
* Draw(func, select='', option='')
  * Draw `func` with a selection `select` and draw options `option`
* DrawWith(hist, func, select='', option='')
  * Draws `func` with a selection `select` and draw options `option` into the given histogram object `hist`
* entry(quiet = False, printfreq=1000, Nentries=None, branchonaccess=True, timeremaining=True, offset=0)
  * Iterates through the
    * quiet turns of printing
    * printfreq sets the number of entries to process before printing the progress
    * Nentries sets the maximum number of entries to process
    * branchonaccess controls if processing is optimised by only setting up a branch when it is first accessed
    * timeremaining prints an estimated time for completion
    * offset sets the entry to start iterating from
  * Returns an integer index for the currently loaded entry
* get(what)
  * Returns the value for `what`, where `what` can be a branch, alias, or function
* getEntry()
  * Returns an integer for the currently loaded entry
* GetEntry(entry)
  * Loads the entry `entry` as the current entry in the tree
* GetEntries(self, selection=None)
  * Returns an integer giving
* getFileStartLength(filename)
  * Returns a two element tuple containing the offset and length of the tree in the given `filename` with respect to the tree chain
* GetMaximum(name)
  * Gets the maximum of value of the branch (or alias) `name`
* GetMinimum(name)
  * Gets the minimum of value of the branch (or alias) `name`
* has(self, what)
  * Returns a boolean stating if the tree contains a branch or alias called `what`
* listBranches()
  * Returns a list of strings containing the names of all the branches in the tree
* removeFriend(friend)
  * Removes the friend tree `friend`
* searchBranch(self, what)
  * Returns a list of branch names containing `what`
* tree()
  * Returns the tree object

#### Example usage ####

        from ic_ttree import TreeWrapper

        tree = TreeWrapper('myfile.root', 'mytree')
        for ientry in tree.entry():
          print tree.Mass

### Types ###

Branches may be of the following types (with predefined constants holding their string constants):

* DOUBLE
  * 'D'
* FLOAT
  * 'F'
* BOOL
  * 'O'
* BYTE
  * 'B'
* UBYTE
  * 'b'
* SHORT
  * 'S'
* USHORT
  * 's'
* INT
  * 'I'
* UINT
  * 'i'
* LONG
  * 'G'
* ULONG
  * 'g'
* LONGLONG
  * 'L'
* ULONGLONG 
  * 'l'
* OBJECT
  * 'object'

#### Usage ####

from ic_ttree import LONGLONG

branch = {'name':'Count', 'type':LONGLONG}
