from setuptools import setup

setup(name='ic_ttree',
      version='0.1',
      description='''Imperial package for manipulating ROOT's TTrees''',
      url='https://gitlab.cern.ch/lhcb-imperial-public/python/ic_ttree',
      author='Michael McCann',
      author_email='m.mccann@imperial.ac.uk',
      license='MIT',
      packages=['ic_ttree'],
      zip_safe=False)
